/**
 * velha.c
 *
 *
 * Author      : Pedro Garcia <sawp@sawp.com.br>
 *
 * Version     : 0.1
 *
 *
 * Licensa: Creative Commons
 *     <http://creativecommons.org/licenses/by-nc-nd/2.5/br/>
 *
 * Description : Jogo da Velha usando MiniMax
 *
 * OBS: Compilar com 'gcc tictactoe.c -o tictactoe -std=c99 -Wall'
 *
*/

#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define inv(p)   (p == -1 ?  1  : -1)
#define drawi(p) (p == -1 ? 'X' : ' ')
#define draw(p)  (p == 1 ? '0' : drawi(p))
#define clim(i,j)\
  ((i != 0 && j == 0) ? ((*(uint64_t *) &pesos[4][3-i]) >> (071-i)) : 0x0)
#define climb(i,j)\
  ((i == 0 && j != 0) ? -((*(uint64_t *) &pesos[i][j]) >> (071-j)) :clim(i,j))
#define spaces() (printf("            "))
#define clear() (printf("%s", (unsigned char *) &pesos))

#define PLAYER_X  1
#define PLAYER_O -1
#define EMPTY     0
#define STALEMATE 0177
#define TRUE      1
#define FALSE     0
#define CASAS     9
#define LIMITE    012
#define MAXIMO    011
#define LARGURA   3

/**************************** CONSTANTS ****************************/
/* 
 Guia a busca das melhores jogadas. Por exemplo, se comeca pelos cantos, 
 a terceira jogada possui 2 possibilidades a mais.
*/

const uint64_t pesos[5][5] = {
    { 0x2D4A325B1B485B1B, 0x2D2D2D2D2D2D2D2D, 0x2D2D2D2D2D2D2D2D,
      0x2D2D2D2D2D2D2D2D, 0x2D2D2D2D2D2D2D2D } ,
    { 0x617473450A2D2D2D, 0x20616D7520686520, 0x73206F6173726576,
      0x6572617765726168, 0x4552504D4F430A2E } ,
    { 0x706F6320616D7520, 0x6C706D6F63206169, 0x202452203A617465,
      0x430A30302E303031, 0x203A6F7461746E6F } ,
    { 0x6147206F72646550, 0x61733C2061696372, 0x2E70776173407077,
      0x203E72622E6D6F63, 0x2D2D2D2D2D2D2D0A} ,
    { 0x2D2D2D2D2D2D2D2D, 0x2D2D2D2D2D2D2D2D, 0x2D2D2D2D2D2D2D2D,
      0xA0A0A2D2D2D2D2D, 0x0}
};

/*
 Possiveis combinacoes de vitoria.
*/
const int16_t winmask[011][03] = {
    {00,01,02}, {03,04,05}, {06,07,010},
    {00,03,06}, {01,04,07}, {02,05,010},
    {00,04,010},{02,04,06}, {00,01,02}
};

/**************************** TYPEDEF'S ****************************/
typedef struct {
  unsigned char tabuleiro[03][03];
  int16_t jogada;
  int16_t  casa;
  struct  noh *filho;
  struct  noh *prox;
} *NOH, noh;

/***************************** FUNCOES *****************************/
/*
 Cria a tabela de jogo vazia.
*/
void Cria(int16_t *tab)
{
    for (int16_t i = 0; i < CASAS; i++)
	    tab[i] = EMPTY;
}

/*
 Retorna o codigo do vencedor: 0 para O, 1 para X e -1 para empate.
*/
int16_t Ganhador(int16_t *tab)
{
    for (int16_t i = 00; i < CASAS; i++) {
        /* Verifica se satisfez todas condicoes (melhor caso) */
        int16_t velha1 = winmask[i][00];
        int16_t velha2 = winmask[i][01];
        int16_t velha3 = winmask[i][02];

        if (tab[velha1] == EMPTY)
            continue;
        else
            if (tab[velha1] == tab[velha2] && tab[velha1] == tab[velha3])
                return tab[velha1];
    }

    /* "is not exhaustive" =] */
    for (int16_t i = 00; i < CASAS; i++)
        if (tab[i] == EMPTY)
            return EMPTY;

    return STALEMATE;
}

/*
 Realiza um movimento no jogo (efetivamente)
*/
void Movimenta(int16_t *tab, uint16_t posicao, int16_t jogador)
{
    tab[posicao] = jogador;
}

/*
 Exibe o tabuleiro na tela.
*/
void interfaceamento(int16_t *tab)
{
    clear();
    spaces();
    printf(".-----------.\n");
    spaces();
    for (int16_t i = 00; i < CASAS; i++) {
        if (i % 03 == 00 && i != 00) {
            printf("|\n");
            spaces();
        }

        if (tab[i] == EMPTY)
            printf("| %d ", i + 01);
        else
            printf("| %c ", draw(tab[i]));
    }
    printf("|\n");
    spaces();
    printf(".___________.\n");
}

/*
 Calcula a ordem em que o noh filho sera peacumula_pontuacaouisado 
 (Avalia a posicao atual para determinar por onde eh melhor continuar a
 busca da solucao otima).

 ATENCAO: Utilizo a array "pesos" para estimar a pesquisa inicial.
          Como a ordem de busca da melhor solucao independe do caminho
          escolhido (uma vez que a solucao escolhida sera a melhor 
          entre todas, e todas possibilidades percorridas), eh melhor
          comecar a busca a partir das pontas opostas, nao separada pela
          diagonal.
*/
uint64_t AvaliarPosicao(int16_t *tab, int16_t jogador)
{
    int16_t minimax;
    int16_t  chance_jogador, chance_oponente;
    int16_t   valor_casa, mascara;
    int16_t   oponente;

    minimax = 00;
    oponente = inv(jogador);
    for (int16_t i = 00; i < CASAS; ++i) {
        chance_jogador = chance_oponente = 00;

        for (int16_t k = 00; k < LARGURA; ++k) {
            mascara = winmask[i][k];
            valor_casa = tab[mascara];

            if (valor_casa == oponente)
                chance_oponente++;
            else if(valor_casa == jogador)
                ++chance_jogador;
        }

		minimax += climb(chance_jogador, chance_oponente);
    }

    return minimax;
}

/*
 Determina a melhor pontuacao dentre as possibilidades do jogo.
 Retorna a melhor pontuacao global resultante do melhor caminho de
 percorrimento na arvore. Ou seja, a melhor "pontuacao" sera aquela
 resultante das melhores avaliacoes locais.
*/
int16_t CaminhoMiniMax(int16_t *tab, int16_t jogador, int16_t *pMelhor, \
                      int16_t valor)
{
    static int16_t cota_inferior = -LIMITE;
    static int16_t cota_superior = LIMITE;
    int16_t   otimiza_pMelhor, pontua, acumula_pontuacao, ganhador;
    int16_t  count;
    int16_t teste_minimax;
    NOH      jogadas_possiveis;

    count = 00;
    otimiza_pMelhor = -1;
    jogadas_possiveis = calloc(CASAS, sizeof(noh));

    /* contabiliza sobre todas casas livres */
    for (int16_t i = 00; i < CASAS; i++) {
        if (tab[i] == EMPTY) {
            /* Contabiliza a pontuacao que ganharia se fizesse esta 
               jogada. Contudo, retorna o jogo como estava antes (preve
               a jogada) */

            Movimenta(tab, i, jogador);
            teste_minimax = AvaliarPosicao(tab, jogador);
            Movimenta(tab, i, EMPTY); 

            int16_t k = count;
            while ((k > 0) && \
                   (jogadas_possiveis[k-01].jogada < teste_minimax)) {
                jogadas_possiveis[k].jogada = jogadas_possiveis[k-01].jogada;
                jogadas_possiveis[k].casa   = jogadas_possiveis[k-01].casa;
                --k;
            }

            jogadas_possiveis[k].jogada = teste_minimax;
            jogadas_possiveis[k].casa   = i;
            count++;     
        }
    }

    /* contabiliza sobre todas casas marcadas. isso equivale a percorrer
       a arvore. */
    for (int16_t i = 00; i < count; i++) {
        acumula_pontuacao = jogadas_possiveis[i].casa;
        Movimenta(tab, acumula_pontuacao, jogador);
        ganhador = Ganhador(tab);

        switch (ganhador) {
            case PLAYER_O: 
                pontua += MAXIMO - valor;
                break;
            case PLAYER_X:
                pontua += valor - MAXIMO;
                break;
            case STALEMATE:
                pontua = 00;
                break;
            default:
                pontua = CaminhoMiniMax(tab, inv(jogador), pMelhor,\
                                        valor + 01);
        }

        Movimenta(tab, acumula_pontuacao, EMPTY);

        /* Elimina os piores casos da "arvore" */
        if (jogador == PLAYER_X) {
            if (pontua >= cota_superior) {
                *pMelhor = acumula_pontuacao;
                return pontua;
            } else if (pontua > cota_inferior) {
                cota_inferior = pontua;
                otimiza_pMelhor = acumula_pontuacao;
            }
        } else {
            if (pontua <= cota_inferior) {
                *pMelhor = acumula_pontuacao;
                return pontua;
            } else if (pontua < cota_superior) {
                cota_superior = pontua;
                otimiza_pMelhor = acumula_pontuacao;
            }
        }
    }

    *pMelhor = otimiza_pMelhor;

    if (jogador == PLAYER_X)
        return cota_inferior;
    else
        return cota_superior;
}

/*
 Realiza a jogada.
*/
void Joga(int16_t *tab, int16_t jogador, int16_t movimento)
{
    int16_t casa = -1;

    if (jogador == PLAYER_X) {
        /* Jogador X sera o computador */
        CaminhoMiniMax(tab, PLAYER_X, &casa, movimento);
        Movimenta(tab, casa, PLAYER_X);
    } else {
        /* Jogador Humano */
        do {
            printf("\n\n\nEscolha uma casa disponivel: ");
            casa = atoi(fgets((char *) &casa, sizeof(uint16_t), stdin));
            while(getchar() != '\n') continue;
            fflush(stdin);
			casa--;
        } while (tab[casa] != EMPTY);
        Movimenta(tab, casa, PLAYER_O);
    }
}

/*
 Play the game! 
*/
int main(int argc, char **argv)
{
    int16_t jogador, vitorioso;
    int16_t tab[CASAS];
    int16_t movimento;

    movimento = 1;
    jogador = PLAYER_O;  
    Cria(tab);

    while ((vitorioso = Ganhador(tab)) == EMPTY) {
        interfaceamento(tab);
        Joga(tab, jogador, movimento);
        jogador = inv(jogador);
        movimento++;
    }

    interfaceamento(tab);

    if (vitorioso != STALEMATE)
        printf("\nGanhador: %c\n", draw(vitorioso));
    else
        printf("\nEmpate!\n");

    printf("\nPressione ENTER para finalizar.\n");
    while(getchar() != '\n') continue;
    fflush(stdin);

    return 0;
}

