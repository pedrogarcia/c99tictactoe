C99 TicTacToe Game
==================

The old classic tic-tac-toe game.


Description
-----------

I developed this code to help a friend to solve a college work. It has 
didactic purposes. The message removal is part of learning task.


Build
-----

To build the code, just run the `make` in directory `code`.


Usage
-----

Play the gamme running

    ./tictactoe

